import tempfile
import os.path
from PIL import Image


class TempFilesController:
    def __init__(self):
        self.temp_path = ''
        self.temp_pathReserved = ''
        self.file = None
        self.fileReserved = None

    def create_temp_files(self):
        self.file = tempfile.NamedTemporaryFile()
        self.fileReserved = tempfile.NamedTemporaryFile()
        self.temp_path = os.path.abspath(self.file.name)
        self.temp_pathReserved = os.path.abspath(self.fileReserved.name)

    def init_temp_file(self, orig_file):
        Image.open(orig_file).convert('RGB').save(self.temp_path, 'JPEG', quality=100)

    def write_changes(self, image):
        os.remove(self.temp_pathReserved)
        os.rename(self.temp_path, self.temp_pathReserved)
        image.save(self.temp_path, 'JPEG', quality=100)

    def close_temp_files(self):
        self.file.close()
        self.fileReserved.close()
        if os.path.exists(self.temp_path):
            os.remove(self.temp_path)
        if os.path.exists(self.temp_pathReserved):
            os.remove(self.temp_pathReserved)

    def undo_redo(self):
        os.rename(self.temp_path, self.temp_path + '_1')
        os.rename(self.temp_pathReserved, self.temp_path)
        os.rename(self.temp_path + '_1', self.temp_pathReserved)