import numpy as np
from PIL import Image, ImageDraw
from collections import namedtuple


class ImageTransformationsController:
    Filter = namedtuple('SharpenFilter', ['name', 'factor', 'bias', 'filter'])

    filter_blur = Filter('blur', 1.0 / 13.0, 0.0, [
        [0, 0, 1, 0, 0],
        [0, 1, 1, 1, 0],
        [1, 1, 1, 1, 1],
        [0, 1, 1, 1, 0],
        [0, 0, 1, 0, 0]
    ])

    filter_sharpen = Filter('sharpen', 1, 0.0, [
        [0, -1, 0],
        [-1, 5, -1],
        [0, -1, 0]
    ])

    filter_edge_detect = Filter('edge_detect', 0.5, 0.0, [
        [0, 1, 0],
        [1, -4, 1],
        [0, 1, 0]
    ])

    filter_emboss = Filter('emboss', 1.5, 0.0, [
        [-2, -1, 0],
        [-1, 1, 1],
        [0, 1, 2]
    ])

    @staticmethod
    def get_image_data(path):
        image = Image.open(path)
        w, h = image.size[0], image.size[1]
        return w, h, np.array(image)

    def __init__(self, temp_files_ctrl):
        self.orig_w = self.orig_h = self.w = self.h = 0
        self.temp_files_ctrl = temp_files_ctrl
        self.original_image_path = ''
        self.temp_image_path = ''

    def connect_new_image(self, image_path):
        self.temp_files_ctrl.init_temp_file(image_path)
        self.original_image_path = image_path
        self.temp_image_path = self.temp_files_ctrl.temp_path
        self.orig_w, self.orig_h, __ = self.get_image_data(self.original_image_path)

    def gray_scale(self, mode):
        if (mode != 'average') and (mode != 'lightness') and (mode != 'luminosity'):
            raise AttributeError('Unknown type of gray scale' + mode)
        __, __, p = self.get_image_data(self.temp_image_path)
        pix = np.copy(p).astype(int)
        r, g, b = pix[:, :, 0], pix[:, :, 1], pix[:, :, 2]
        if mode == 'average':
            m = (r + g + b) / 3
        if mode == 'lightness':
            m = (np.maximum(r, g, b) + np.minimum(r, g, b)) / 2
        if mode == 'luminosity':
            m = 0.21 * r + 0.72 * g + 0.07 * b
        pix[:, :, 0], pix[:, :, 1], pix[:, :, 2] = m, m, m
        pix = np.clip(pix[:, :, :], 0, 255)
        self.temp_files_ctrl.write_changes(Image.fromarray(pix.astype('uint8')))

    def brightness(self, factor):
        w, h, p = self.get_image_data(self.temp_image_path)
        pix = np.copy(p).astype(int)
        pix += factor
        pix = np.clip(pix[:, :, :], 0, 255)
        self.temp_files_ctrl.write_changes(Image.fromarray(pix.astype('uint8')))

    def undo_redo(self):
        self.temp_files_ctrl.undo_redo()

    def noise(self, factor=50):
        w, h, p = self.get_image_data(self.temp_image_path)
        pix = np.copy(p).astype(int)
        r, g, b = pix[:, :, 0], pix[:, :, 1], pix[:, :, 2]
        rand = np.random.randint(-factor, factor, r.size).reshape(h, w)
        r, g, b = r + rand, g + rand, b + rand
        pix[:, :, 0], pix[:, :, 1], pix[:, :, 2] = r, g, b
        pix = np.clip(pix[:, :, :], 0, 255)
        self.temp_files_ctrl.write_changes(Image.fromarray(pix.astype('uint8')))

    def blur(self):
        return self.apply_filter(self.filter_blur)

    def sharpen(self):
        return self.apply_filter(self.filter_sharpen)

    def edge_detect(self):
        return self.apply_filter(self.filter_edge_detect)

    def emboss(self):
        return self.apply_filter(self.filter_emboss)

    def apply_filter(self, filter_matrix):
        image = Image.open(self.temp_image_path)
        w, h, p = image.size[0], image.size[1], image.load()
        new_image = Image.new('RGB', [w, h], (255, 255, 255))
        draw = ImageDraw.Draw(new_image)
        for x in range(w):
            for y in range(h):
                r, g, b = 0.0, 0.0, 0.0

                for fy in range(len(filter_matrix.filter)):
                    for fx in range(len(filter_matrix.filter[0])):
                        im_x = (x - len(filter_matrix.filter[0]) / 2 + fx + w) % w
                        im_y = (y - len(filter_matrix.filter) / 2 + fy + h) % h

                        r += p[im_x, im_y][0] * filter_matrix.filter[fy][fx]
                        g += p[im_x, im_y][1] * filter_matrix.filter[fy][fx]
                        b += p[im_x, im_y][2] * filter_matrix.filter[fy][fx]

                r = min(max(int(filter_matrix.factor * r + filter_matrix.bias), 0), 255)
                g = min(max(int(filter_matrix.factor * g + filter_matrix.bias), 0), 255)
                b = min(max(int(filter_matrix.factor * b + filter_matrix.bias), 0), 255)
                draw.point((x, y), (r, g, b))
        self.temp_files_ctrl.write_changes(new_image)
        del draw

    def invert(self):
        __, __, p = self.get_image_data(self.temp_image_path)
        r, g, b = p[:, :, 0], p[:, :, 1], p[:, :, 2]
        p[:, :, 0] = 255 - r
        p[:, :, 1] = 255 - g
        p[:, :, 2] = 255 - b
        self.temp_files_ctrl.write_changes(Image.fromarray(p.astype('uint8')))

    def sepia(self):
        __, __, p = self.get_image_data(self.temp_image_path)
        pix = np.copy(p).astype(int)
        depth = 15
        r, g, b = pix[:, :, 0], pix[:, :, 1], pix[:, :, 2]
        mid = (r + g + b) // 3
        r, g, b = mid + depth * 2, mid + depth, mid
        pix[:, :, 0], pix[:, :, 1], pix[:, :, 2] = r, g, b
        pix = np.clip(pix[:, :, :], 0, 255)
        self.temp_files_ctrl.write_changes(Image.fromarray(pix.astype('uint8')))

    def rotate_left(self):
        __, __, p = self.get_image_data(self.temp_image_path)
        pix = np.copy(p).astype(int)
        r, g, b = np.rot90(pix[:, :, 0]), np.rot90(pix[:, :, 1]), np.rot90(pix[:, :, 2])
        ret = np.stack((r, g, b), 2)
        self.temp_files_ctrl.write_changes(Image.fromarray(ret.astype('uint8')))

    def rotate_right(self):
        __, __, p = self.get_image_data(self.temp_image_path)
        pix = np.copy(p).astype(int)
        r, g, b = np.rot90(pix[:, :, 0], -1), np.rot90(pix[:, :, 1], -1), np.rot90(pix[:, :, 2], -1)
        ret = np.stack((r, g, b), 2)
        self.temp_files_ctrl.write_changes(Image.fromarray(ret.astype('uint8')))
