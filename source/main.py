#!/usr/bin/env python3
# author: Denis Karlov
# title: Image editor

from gui import *
from temp_file import *
from image_transformations import *


if __name__ == "__main__":
    try:
        temp_images_ctrl = TempFilesController()
        temp_images_ctrl.create_temp_files()
        transform_ctrl = ImageTransformationsController(temp_images_ctrl)
        gui = Gui(transform_ctrl)
        gui.run()
    finally:
        temp_images_ctrl.close_temp_files()