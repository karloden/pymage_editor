import os

from PyQt5 import QtWidgets, uic
from PyQt5.QtWidgets import QDesktopWidget
from PIL import Image
from functools import partial

from window import *


class Gui:
    def __init__(self, transform_ctrl):  # injecting controller's instances
        self.app = QtWidgets.QApplication([])
        self.win = Window(self)
        try:
            ui = open('main.ui')
        except IOError:
            print('Interface XML file not found')
            exit(1)
        else:
            with ui:
                uic.loadUi(ui, self.win)
        self.opened_filename = ''
        self.opened_filedir = os.path.curdir
        self.tmp_filepath = ''
        self.transform_ctrl = transform_ctrl
        self.image_label = None
        self.processing = self.redone = self.changed = self.first_change = False
        self._init_ui()

    def _get_widget(self, name):
        return self.win.findChild(QtWidgets.QAction, name)

    def _toggle_progress_displaying(self):
        if self.processing:
            self.win.findChild(QtWidgets.QLabel, 'procLabel').show()
        else:
            self.win.findChild(QtWidgets.QLabel, 'procLabel').hide()
        self.win.findChild(QtWidgets.QLabel, 'procLabel').repaint()
        self.processing = not self.processing

    def _init_ui(self):
        self._title_update()
        self._toggle_progress_displaying()
        screen_size = QDesktopWidget().screenGeometry()
        win_size = self.win.geometry()
        self.win.move((screen_size.width() - win_size.width()) / 2, (screen_size.height() - win_size.height()) / 2)
        self._get_widget('actionOpen').triggered.connect(self._open)
        self._get_widget('actionQuit').triggered.connect(self.win.close)
        self._get_widget('actionSave').triggered.connect(partial(self.save_as, quick=True))
        self._get_widget('actionSaveAs').triggered.connect(self.save_as)
        self._get_widget('actionImageProperties').triggered.connect(self.show_image_properties)
        self._get_widget('actionUndo').triggered.connect(self._undo_redo)
        self._get_widget('actionRedo').triggered.connect(self._undo_redo)
        self._get_widget('actionZoomIn').triggered.connect(partial(self.zoom, 1.2))

        self._get_widget('actionZoomOut').triggered.connect(partial(self.zoom, 0.83))

        self._get_widget('actionRotateLeft').triggered.connect(
            partial(self.image_transformation, self.transform_ctrl.rotate_left))

        self._get_widget('actionRotateRight').triggered.connect(
            partial(self.image_transformation, self.transform_ctrl.rotate_right))

        self._get_widget('actionBrightnessMore').triggered.connect(
            partial(self.image_transformation, partial(self.transform_ctrl.brightness, 20)))

        self._get_widget('actionBrightnessLess').triggered.connect(
            partial(self.image_transformation, partial(self.transform_ctrl.brightness, -20)))

        self._get_widget('actionNoise').triggered.connect(
            partial(self.image_transformation, self.transform_ctrl.noise))

        self._get_widget('actionBlur').triggered.connect(
            partial(self.image_transformation, self.transform_ctrl.blur))

        self._get_widget('actionSharpen').triggered.connect(
            partial(self.image_transformation, self.transform_ctrl.sharpen))

        self._get_widget('actionEdgeDetect').triggered.connect(
            partial(self.image_transformation, self.transform_ctrl.edge_detect))

        self._get_widget('actionEmboss').triggered.connect(
            partial(self.image_transformation, self.transform_ctrl.emboss))

        self._get_widget('actionGrayscaleAverage').triggered.connect(
            partial(self.image_transformation, partial(self.transform_ctrl.gray_scale, 'average')))

        self._get_widget('actionGrayscaleLightness').triggered.connect(
            partial(self.image_transformation, partial(self.transform_ctrl.gray_scale, 'lightness')))

        self._get_widget('actionGrayscaleLuminosity').triggered.connect(
            partial(self.image_transformation, partial(self.transform_ctrl.gray_scale, 'luminosity')))

        self._get_widget('actionInvert').triggered.connect(
            partial(self.image_transformation, self.transform_ctrl.invert))

        self._get_widget('actionSepia').triggered.connect(
            partial(self.image_transformation, self.transform_ctrl.sepia))

    def _enable_menu_entries(self):
        self._get_widget('actionSaveAs').setEnabled(True)
        self._get_widget('actionImageProperties').setEnabled(True)
        self._get_widget('actionZoomIn').setEnabled(True)
        self._get_widget('actionZoomOut').setEnabled(True)
        self._get_widget('actionRotateLeft').setEnabled(True)
        self._get_widget('actionRotateRight').setEnabled(True)
        self._get_widget('actionBrightnessMore').setEnabled(True)
        self._get_widget('actionBrightnessLess').setEnabled(True)
        self._get_widget('actionNoise').setEnabled(True)
        self._get_widget('actionBlur').setEnabled(True)
        self._get_widget('actionSharpen').setEnabled(True)
        self._get_widget('actionEdgeDetect').setEnabled(True)
        self._get_widget('actionEmboss').setEnabled(True)
        self._get_widget('actionGrayscaleAverage').setEnabled(True)
        self._get_widget('actionGrayscaleLightness').setEnabled(True)
        self._get_widget('actionGrayscaleLuminosity').setEnabled(True)
        self._get_widget('actionInvert').setEnabled(True)
        self._get_widget('actionSepia').setEnabled(True)
        self._get_widget('actionUndo').setEnabled(False)
        self._get_widget('actionRedo').setEnabled(False)

    def run(self):
        self.win.show()
        return self.app.exec_()

    def _title_update(self):
        self.title = 'Pymage editor - ' + self.opened_filename + ' ' + ('*' if self.changed else '')
        self.win.setWindowTitle(self.title)

    def show_close_image_dialog(self):
        msg_box = QtWidgets.QMessageBox(QtWidgets.QMessageBox.Warning, 'File was modified',
                                        'Your image was modified, would you like to save it?', parent=self.win)
        msg_box.addButton('Save', QtWidgets.QMessageBox.YesRole)
        msg_box.addButton('Don\'t save', QtWidgets.QMessageBox.NoRole)
        msg_box.addButton('Cancel', QtWidgets.QMessageBox.RejectRole)
        return msg_box.exec_()

    def _open(self, file_path=''):
        if self.changed:
            result = self.show_close_image_dialog()
            if result == 0:
                self.save_as(quick=True, im_close=True)
            elif result == 2:
                return
        if not file_path:
            file_path, __ = QtWidgets.QFileDialog.getOpenFileName(self.win, 'Open File', self.opened_filedir)
        if not file_path:
            return
        self.opened_filedir = os.path.dirname(file_path)
        self.transform_ctrl.connect_new_image(file_path)
        self.tmp_filepath = self.transform_ctrl.temp_image_path
        self.image_label = ImageViewer(self.win, self.tmp_filepath)
        self.scroll_area = self.win.findChild(QtWidgets.QScrollArea, 'scrollArea')
        self.scroll_area.setWidget(self.image_label)
        self.opened_filename = os.path.basename(file_path)
        self.redone = self.changed = self.first_change = False
        self._title_update()
        self._enable_menu_entries()
        self.image_label.update()

    def save_as(self, quick=False, im_close=False, save_to=''):
        if quick and not save_to:
            save_to = os.path.join(self.opened_filedir, self.opened_filename)
        if not quick:
            save_to, __ = QtWidgets.QFileDialog.getSaveFileName(self.win, 'Save image', self.opened_filedir,
                                                                'Image files (*.jpg *.bmp *.png *.gif)')
        if not save_to:
            return
        self.first_change = self.changed = False
        image = Image.open(self.tmp_filepath)
        image.save(save_to)
        if im_close:
            return
        self._get_widget('actionSave').setEnabled(False)
        self._open(save_to)

    def show_image_properties(self):
        w, h = self.transform_ctrl.orig_w, self.transform_ctrl.orig_h
        opened_file_path = os.path.join(self.opened_filedir, self.opened_filename)
        size_in_bytes = os.path.getsize(self.tmp_filepath)
        suffixes = ['B', 'KB', 'MB']
        i = 0
        while size_in_bytes >= 1024 and i < len(suffixes) - 1:
            size_in_bytes /= 1024.
            i += 1
        f = ('%.2f' % size_in_bytes).rstrip('0').rstrip('.')
        size = '%s %s' % (f, suffixes[i])
        disclamer = '\n\n(original file info)' if self.changed else ''
        info = 'Path: %s\n\nSize in pixels: %d x %d \n\nFile size: %s %s' % (opened_file_path, w, h, size, disclamer)
        QtWidgets.QMessageBox.information(self.win, self.opened_filename, info, QtWidgets.QMessageBox.Ok)

    def zoom(self, factor):
        if self.image_label.scale_image(factor):
            scroll_bar = self.scroll_area.horizontalScrollBar()
            scroll_bar.setValue(int(factor * scroll_bar.value() + ((factor - 1) * scroll_bar.pageStep() / 2)))
            scroll_bar = self.scroll_area.verticalScrollBar()
            scroll_bar.setValue(int(factor * scroll_bar.value() + ((factor - 1) * scroll_bar.pageStep() / 2)))

    def _undo_redo(self):
        if not self.redone and self.changed and self.first_change:
            self.changed = self.first_change = False
        elif self.redone and not self.changed:
            self.changed = self.first_change = True
        self._get_widget('actionUndo').setEnabled(self.redone)
        self._get_widget('actionRedo').setEnabled(not self.redone)
        self.redone = not self.redone
        self.transform_ctrl.undo_redo()
        self._title_update()
        self.image_label.update()

    def image_transformation(self, fun):
        self._toggle_progress_displaying()
        fun()
        self._get_widget('actionRedo').setEnabled(False)
        self._get_widget('actionUndo').setEnabled(True)
        if self.first_change:
            self.first_change = False
        if not self.changed:
            self.first_change = True
        self.redone = False
        self.changed = True
        self.image_label.update()
        self._title_update()
        self.win.findChild(QtWidgets.QAction, 'actionSave').setEnabled(True)
        self._toggle_progress_displaying()
