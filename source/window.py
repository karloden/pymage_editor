from PyQt5 import QtWidgets, QtCore
from PyQt5.QtGui import QPixmap


class Window(QtWidgets.QMainWindow):
    def __init__(self, gui):
        super().__init__()
        self.gui = gui

    def closeEvent(self, event):
        if not self.gui.changed:
            event.accept()
            return
        result = self.gui.show_close_image_dialog()
        if result == 2:
            event.ignore()
        elif result == 0:
            self.gui.save_as(quick=True, im_close=True)
            event.accept()
        elif result == 1:
            event.accept()


class ImageViewer(QtWidgets.QLabel):
    def __init__(self, parent, tmp_path):
        super().__init__()
        self.parent = parent
        self.tmp_path = tmp_path
        self.scale_factor = 1.0
        self.current_w = self.current_h = 0
        self.update()

    def update(self):
        self.setAlignment(QtCore.Qt.AlignCenter)
        pix_map = QPixmap(self.tmp_path)
        self.current_w, self.current_h = pix_map.width(), pix_map.height()
        w = self.parent.width() - 20
        h = self.parent.height() - 48
        if h < pix_map.height() or w < pix_map.width():
            if self.current_w < self.current_h:
                self.current_w = int((self.current_w * h) / self.current_h)
                self.current_h = h
                if self.current_w > w:
                    self.current_h = int((self.current_h * w) / self.current_w)
                    self.current_w = w
            else:
                self.current_h = int((self.current_h * w) / self.current_w)
                self.current_w = w
                if self.current_h > h:
                    self.current_w = int((self.current_w * h) / self.current_h)
                    self.current_h = h
        self.setPixmap(pix_map.scaled(self.current_w, self.current_h, QtCore.Qt.KeepAspectRatio))

    def scale_image(self, factor):
        if ((self.current_h > 10000 or self.current_w > 10000) and factor > 1) or factor == 1 or \
                (self.current_h < self.parent.height() and factor < 1
                 and self.current_w < self.parent.width()):
            return False
        self.current_w = int(factor * self.current_w)
        self.current_h = int(factor * self.current_h)
        pix_map = QPixmap(self.tmp_path)
        self.setPixmap(pix_map.scaled(self.current_w, self.current_h, QtCore.Qt.KeepAspectRatio))
        return True